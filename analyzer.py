from pathlib import Path
import time
import json

BASE_DIR = Path(__file__).resolve().parent

LAST_DATE_FILE = BASE_DIR.joinpath('.lastdate')

DATA_FILE = BASE_DIR.joinpath('stats.json')

LOG_DIR = Path('/var/log/3proxy')


def split_line(line):
    data = line.split()

    try:
        date = data[0].split('.')
        date = '.'.join([date[2], date[1], date[0], date[3]])

        return {
            'date': date,
            'username': data[3],
            'ip': data[5].split(':')[0],
            'port': data[1].split('.')[-1],
            'out': int(data[9]),
            'in': int(data[11]),
            'line': line
        }
    except:
        pass


def process_log(log_path):
    pause = .2
    pauses = 0
    wait = 10

    with open(log_path, 'r') as log:
        while True:
            line = log.readline()
            if not line:
                time.sleep(pause)
                pauses += pause
                if pauses >= wait:
                    break
            else:
                pauses = 0
                yield split_line(line)


def update_data_file(data):
    with open(DATA_FILE, 'w') as f:
        f.write(json.dumps(data))


def save_last_date(date):
    with open(LAST_DATE_FILE, 'w') as f:
        f.write(date)


def get_last_date():
    try:
        with open(LAST_DATE_FILE, 'r') as f:
            return f.read().strip()
    except FileNotFoundError:
        return ''


def get_latest_log():
    def get_mtime(f):
        return f.stat().st_mtime

    log = None
    for log_path in LOG_DIR.iterdir():
        if log_path.is_file() and log_path.name.startswith('log.'):
            if log is None or get_mtime(log_path) > get_mtime(log):
                log = log_path

    return log


def main():
    if not DATA_FILE.exists():
        update_data_file({})

    with open(DATA_FILE, 'r') as data_file:
        try:
            stats = json.load(data_file)
        except ValueError:
            update_data_file({})
            stats = {}

    last_date = get_last_date()

    while True:
        log_path = get_latest_log()

        print('Log:', log_path)

        if log_path is None:
            time.sleep(3)
            continue

        data = None
        errors = 0
        i = 0

        for data in process_log(log_path):
            if not data:
                errors += 1
                continue
            if data['date'] <= last_date:
                continue

            i += 1
            date = data['date'].rsplit('.', 1)[0]

            if date not in stats:
                stats[date] = {}

            try:
                stats[date][data['port']][0] += data['out']
                stats[date][data['port']][1] += data['in']
            except KeyError:
                stats[date][data['port']] = [data['out'], data['in']]

            if i % 100 == 0:
                update_data_file(stats)
                last_date = data['date']
                save_last_date(data['date'])

        if data:
            update_data_file(stats)
            last_date = data['date']
            save_last_date(data['date'])

        print(errors)


main()
