import argparse
import json
from datetime import date, timedelta
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent

DATA_FILE = BASE_DIR.joinpath('stats.json')

UNITS = {
    'MB': 1e+6,
    'GB': 1e+9,
    'B': 1
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--days', type=int, default=7)
    parser.add_argument('--units', default='GB')
    parser.add_argument('--count', type=int, default=10)
    return parser.parse_args()


def main():
    args = parse_args()

    dates = []

    for i in range(args.days):
        day = date.today() - timedelta(days=i)
        dates.append(day.strftime('%Y.%m.%d'))

    print('Dates:', dates[-1], '-', dates[0])

    with open(DATA_FILE, 'r') as f:
        stats = json.load(f)

    counter = {}

    for dt in dates:
        if dt in stats:
            for port, values in stats[dt].items():
                try:
                    counter[port][0] += values[0]
                    counter[port][1] += values[1]
                except KeyError:
                    counter[port] = values

    for values in counter.values():
        values.append(values[0] + values[1])

    data = sorted(counter.items(), key=lambda x: x[1][2], reverse=True)[:args.count]
    units = args.units.upper()
    if units not in UNITS:
        units = 'GB'
    devider = UNITS[units]

    def output(*args):
        print('{:<16}{:<16}{:<16}{:<16}'.format(*args))

    output('PORT', f'TOTAL ({units})', 'OUT', 'IN')

    for port, values in data:
        total = round(values[2] / devider, 2)
        out = round(values[0] / devider, 2)
        in_ = round(values[1] / devider, 2)

        output(port, total, out, in_)


main()
